//
// Created by Matthew on 03.01.2022.
//

#include "test.h"
#include "mem.h"
#include "mem_internals.h"

void* heap;

enum test_status test_init(){
    heap = heap_init(8484);
    if (heap){
        return PASSED_TEST;
    }
    return ALLOCATE_ERROR;
}

//Обычное успешное выделение памяти
enum test_status test1() {
    printf("TEST1\nDefault malloc\n");
    if (!heap){
        return ALLOCATE_ERROR;
    }
    debug_heap(stdout, heap);
    printf("Allocated 422\n");
    void* block_addr = _malloc(422, heap);
    struct block_header *block = block_get_header(block_addr);
    if (block->capacity.bytes != 422) {
        printf("Test isn't passed!\n");
        return ALLOCATE_ERROR;
    }
    if (block->is_free) {
        printf("Test isn't passed!\n");
        return ALLOCATE_ERROR;
    }
    printf("Test1 passed!\n");
    debug_heap(stdout, heap);
    printf("\n\n");
    _free(block_addr);
    return PASSED_TEST;
}

//Освобождение одного блока из нескольких выделенных.
enum test_status test2() {
    printf("TEST2\nFree one block after _malloc!\n");


    if (!heap){
        return ALLOCATE_ERROR;
    }

    void *block_to_test = _malloc(1555, heap);
    struct block_header *our_block = block_get_header(block_to_test);
    debug_heap(stdout, heap);
    printf("Try to free our block\n");
    _free(block_to_test);
    if (our_block->capacity.bytes == 1555) {
        printf("Test isn't passed!\n");
        return FREE_ERROR;
    }
    if (!our_block->is_free) {
        printf("Test isn't passed!\n");
        return FREE_ERROR;
    }
    printf("Test2 is passed!\n");
    debug_heap(stdout, heap);
    printf("\n\n");
    return PASSED_TEST;
}


//Освобождение двух блоков из нескольких выделенных.
enum test_status test3() {
    printf("TEST3\nFree two blocks after _malloc!\n");

    if (!heap){
        return ALLOCATE_ERROR;
    }
    void *block_to_test1 = _malloc(1500, heap);
    void *block_to_test2 = _malloc(2000, heap);
    struct block_header *bl1 = block_get_header(block_to_test1);
    struct block_header *bl2 = block_get_header(block_to_test2);
    debug_heap(stdout, heap);
    printf("Try to free first block!\n");
    _free(block_to_test1);
    debug_heap(stdout, heap);
    if (!bl1->is_free) {
        printf("Test isn't passed! First block isn't free!\n");
        return FREE_ERROR;
    }

    printf("Try to free second block!\n");
    _free(block_to_test2);
    if (!bl2->is_free || bl2->capacity.bytes == 2000) {
        printf("Test isn't passed! Second block isn't free!\n");
        return FREE_ERROR;
    }

//    Соединяем первый блок с последним после освобождения второго, так как при освобождении первого следующий блок был занят, и он не был соединен

    try_merge_with_next(bl1);
    debug_heap(stdout, heap);
    printf("Test3 is passed!\n");
    printf("\n\n");
    return PASSED_TEST;
}

//Память закончилась, новый регион памяти расширяет старый.
enum test_status test4() {
    printf("TEST4\nRegion is over. New region is growing up! \n");

    if (!heap){
        return ALLOCATE_ERROR;
    }
    void *first_malloced = _malloc(4242, heap);
    struct block_header *bl1 = block_get_header(first_malloced);
    printf("The first block is allocated!\n");
    debug_heap(stdout, heap);
    printf("Allocate the second block!\n");

    void *second_malloced = _malloc(12500, heap);
    struct block_header *bl2 = block_get_header(second_malloced);

    if (!bl1->next->is_free || bl1->capacity.bytes == 12500) {
        printf("Test4 isn't passed!\n");
        return ALLOCATE_ERROR;
    }

    if (bl2->is_free || bl2->capacity.bytes != 12500) {
        printf("Test4 isn't passed!\n");
        return ALLOCATE_ERROR;
    }

    debug_heap(stdout, heap);
    printf("Test4 passed!\n");
    _free(first_malloced);
    _free(second_malloced);
    return PASSED_TEST;

}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
enum test_status test5() {
    printf("TEST5\nRegion is over. New region is growing up in another place cause the place after overed region isn't free! \n");

    if (!heap){
        return ALLOCATE_ERROR;
    }

    debug_heap(stdout, heap);
    void *test_block_1 = _malloc(2000, heap);
    struct block_header *bl1 = block_get_header(test_block_1);

    void *bl = bl1 + 12500;

    void *mmaped = mmap(bl, 1000, PROT_READ | PROT_WRITE, MAP_FIXED | MAP_PRIVATE, 0, 0);

    if (!mmaped) return ALLOCATE_ERROR;

    debug_heap(stdout, heap);

    _malloc(14500, heap);
    debug_heap(stdout, heap);
    return PASSED_TEST;

}



