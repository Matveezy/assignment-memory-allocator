//
// Created by Matthew on 03.01.2022.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#include "stdbool.h"
bool try_merge_with_next(struct block_header *block);

enum test_status{
    PASSED_TEST,
    NOT_PASSED_TEST,
    ALLOCATE_ERROR,
    FREE_ERROR
};
enum test_status test_init();
enum test_status test1();
enum test_status test2();
enum test_status test3();
enum test_status test4();
enum test_status test5();
#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
