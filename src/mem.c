#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

/**
 * Метод , вычисляющий размер блока, зная его вместимость.
 * Наш блок состоит из двух частей : заголовка и самих данных.
 * Вместимость - сколько байт вмещает в себя секция "данные" в блоке , а вот размер определяется как сумма
 * вместимости и offsetof(struct block_header , contents) , то есть попросту сумма размера заголовка и данных.
 * @param cap
 * @return
 */
extern inline block_size size_from_capacity(block_capacity cap);

/**
 * Метод - антогонист вышенаписанному. Вычисляем вместимость секции данных блока по размеру всего блока.
 * @param sz
 * @return
 */
extern inline block_capacity capacity_from_size(block_size sz);

/**
 * Метод, проверяющий хватит ли вместимости блока для запрошенного кол-ва байт.
 * @param query Сколько хотим заполнить байт
 * @param block Структура , из которой будет доставать вместимость блока
 * @return
 */
static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }


static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

/**
 * Инициализация блока
 * @param addr Адрес заголовка блока
 * @param block_sz Запрашиваемый размер
 * @param next Адрес на следуюзий блок
 */
static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

/**
 * Метод , который не дает задать размер региона(кучи) , меньше минимального (который равен 4096 * 2 байт)
 * @param Запрашиваемый размер кучи
 * @return Запрашиваемый нами размер(в случае, если он больше минимального) , иначе - минимальный размер
 */
static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

/**
 * Метод, проверяющий валидность кучи
 * @param r
 * @return
 */
extern inline bool region_is_invalid(const struct region *r);

/**
 * Метод-обертка системного вызова mmap - который выделяет нам виртуальную память
 * @param addr Желаемый адрес начала (может сработать, а может и нет). В случае , если не сработает - метод выделит
 * нам память, начиная с адреса, который сам выберет
 * @param length Сколько нужно выделить памяти
 * @param additional_flags Флаги
 * @return Ссылка на начало кучи(региона)
 */
static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {

//    Получаем размер нашего региона(вдруг пользователь ввел слишком маленький размер)
    query = region_actual_size(query);
    struct region region;

//    Выделяем память под кучу(регион)
    void *new_address = map_pages(addr, query, MAP_FIXED);;

//    Проверяем на результат выделения памяти
    if (new_address == MAP_FAILED) {
        new_address = map_pages(addr, query, 0);
        region = (struct region) {.addr = new_address, .size = query, .extends=false};
    } else {
        region = (struct region) {.addr = new_address, .size = query, .extends = false};
    }

//    Так как адрес на начало кучи(региона) должен указывать на заголовок первого блока, мы сразу же инициализируем первый блок
    block_init(new_address, (block_size) {.bytes  = query}, NULL);
    return region;
}

static void *block_after(struct block_header const *block);

/**
 * Метод, инициализирующий кучу
 * @param initial Желаемый размер кучи
 * @return Указатель на начало кучи
 */
void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

/**
 * Метод , проверяющий , разделяем ли этот блок
 * Что тут происходит:
 * !!! "offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY" - размер минимально возможного блока !!!
 * Мы проверяем , если кол-во запрашиваемых нами байт в сумме с минимальным возможным блоком меньше или равно, чем вместимость данного блока,
 * тогда можно разделить блок(так как разделенный блок так же должен будет иметь две секции - заголовок и данные)
 * @param block Сссылку на проверяемый блок
 * @param query Сколько байт мы хотим поместить
 * @return Результат
 */
static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}


static bool split_if_too_big(struct block_header *block, size_t query) {

//     Проверяем , разделяемый ли это блок
    if (!block_splittable(block, query)) return false;

//    По архитектуре , если размер блока слишком большой для нашего запроса , мы разделяем его на два блока -
//    первый размером n байт(сколько мы запросили) , второй - оставшиеся кол-во байт
    block_size second_block = (block_size) {.bytes = size_from_capacity(block->capacity).bytes - query};

//    На этом шаге мы уже условно разделили наш большой блок на два,
//    здесь мы нашей первой части(как раз той, где будут лежать n запрошенных байт) задали вместимость n байт
    block->capacity.bytes = query;

//    Нам нужно знать адрес начала второго блока
    void *sec_block = block_after(block);

//    Инициализируем второй блок(на данный момент он пустой)
    block_init(sec_block, second_block, NULL);

//    У первого блока устанавливаем ссылку на следующий блок(то есть на только что инициализированный)
    block->next = sec_block;

    return true;
}


/*  --- Слияние соседних свободных блоков --- */
/**
 * Метод, возвращающий адрес блока , cледующего за данным
 * @param block Данный блок
 * @return Адрес начала идущего за данным блока
 */
static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

/**
 * Метод , проверяющий , идут ли блоки друг за другом
 * @param fst блок из начала пары, которую мы хотим проверить
 * @param snd конец пары
 * @return результат
 */
static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

/**
 * Метод , проверяющий , можно ли соединить данные блоки, то есть проверяющий, свободны ли они оба и идут ли друг за другом
 * @param fst блок из начала пары, которую мы хотим проверить
 * @param snd конец пары
 * @return результат
 */
static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

/**
 * Метод, соединяющий два блока - то есть
 * берет второй блок , присоединяет его к первому , после чего у нас остается первый(увеличенный) блок
 * @param block Блок, который мы хотим соединить со следудющим после него
 * @return Результат(получилось или нет)
 */
bool try_merge_with_next(struct block_header *block) {
//    Если у блока банально нет ссылки на следующий блок или они не соединяемые , возвращаем false
    if (!block->next || !mergeable(block, block->next)) return false;

    block->capacity.bytes = block->capacity.bytes + block->next->capacity.bytes;
    block->next = block->next->next;
    return true;

}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

/**
 * Метод,в которым мы пытаемся найти свободный блок
 * @param block
 * @param sz
 * @return
 */
static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_header *current = block;
    struct block_header *following = NULL;

    while (current) {
//        Если блок свободен и подходит нам по вместимости , то используем его
        if (current->is_free && block_is_big_enough(sz, current)) {
            return (struct block_search_result) {
                    .type = BSR_FOUND_GOOD_BLOCK,
                    .block = current
            };
//            Если блок свободен и не подходит нам по вместимости , то проверяем , можно ли нам замержить его за следующим
//            В случае успеха - используем его
        } else if (current->is_free && try_merge_with_next(current) && (current->capacity.bytes>=sz)) {
            return (struct block_search_result) {
                    .type = BSR_FOUND_GOOD_BLOCK,
                    .block = current
            };
        }
        following = current;
        current = current->next;
    }

//    Иначе возвращаем статус о том, что прошлись по всем блокам и не нашли подходящего(структура хранит ссылку на последний блок)
    return (struct block_search_result) {
            .type = BSR_REACHED_END_NOT_FOUND,
            .block = following
    };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    return find_good_or_last(block, query);
}

/**
 * Метод, который наращивает кучу сверху уже имеющейся
 * @param last Адрес последнего блока
 * @param query Сколько байт мы хотим заполнить
 * @return Адрес заголовка блока, который находится в начале нашей нарощенной кучи
 */
static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    block_size size_block = size_from_capacity(last->capacity);
    const void* addr = (void*) (last->contents + size_block.bytes);
    struct region new_region = alloc_region(addr, query);

    last->next = new_region.addr;
    if (try_merge_with_next(last)) return last;
    return last->next;

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    struct block_search_result res = try_memalloc_existing(query, heap_start);
    if (res.type == BSR_REACHED_END_NOT_FOUND) {
        grow_heap(res.block, query);
        res = try_memalloc_existing(query, heap_start);
    }
    if (res.type != BSR_FOUND_GOOD_BLOCK) return NULL;
    split_if_too_big(res.block, query);
    res.block->is_free = false;
    return res.block;

}

void *_malloc(size_t query , void* heap_start) {
    struct block_header *const addr = memalloc(query, (struct block_header *) heap_start);
    if (addr) return addr->contents;
    else return NULL;
}

struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (header){
        try_merge_with_next(header);
        header = header->next;
    }
}

